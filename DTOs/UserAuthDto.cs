﻿using control_almacen_api.Domain;

namespace control_almacen_api.DTOs
{
    public class UserAuthDto
    {
        public string Token { get; set; }
        public User Usuario { get; set; }
    }
}

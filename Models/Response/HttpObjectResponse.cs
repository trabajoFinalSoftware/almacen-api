﻿
namespace control_almacen_api.Models.Response
{
    public class HttpObjectResponse: HttpResponse
    {
        public string Status { get; set; } = "ok";
        public object Result { get; set; }
    }
}

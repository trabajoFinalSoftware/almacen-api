﻿
namespace control_almacen_api.Models.Response
{
    public class HttpErrorResponse: HttpResponse
    {
        public override string Status { get; set; } = "error";
        public string Error { get; set; }
    }
}

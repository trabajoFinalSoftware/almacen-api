﻿
namespace control_almacen_api.Models.Response
{
    public class HttpArrayResponse: HttpResponse
    {
        public override string Status { get; set; } = "ok";
        public object Results { get; set; }
        public int Total { get; set; }
    }
}

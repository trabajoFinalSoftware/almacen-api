﻿using control_almacen_api.Context;
using control_almacen_api.Domain;
using System.Collections.Generic;
using System.Linq;

namespace control_almacen_api.Seeders
{
    public class ActionLogSeeder
    {
        public static void Seed(AppDbContext context)
        {
            if (context.ActionLogs.Any())
                return;

            List<ActionLog> actionLogs = new List<ActionLog>
            {
                new ActionLog
                {
                    Id = "1",
                    StaticCode = 1,
                    Description = "Entrada",
                    Active = true
                },
                new ActionLog
                {
                    Id = "2",
                    StaticCode = 2,
                    Description = "Salida",
                    Active = true
                }
            };

            actionLogs.ForEach(a => context.ActionLogs.Add(a));
            context.SaveChanges();
        }
    }
}

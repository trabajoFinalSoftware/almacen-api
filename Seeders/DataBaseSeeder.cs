﻿using control_almacen_api.Context;

namespace control_almacen_api.Seeders
{
    public class DataBaseSeeder
    {
        public static void Seed(AppDbContext context)
        {
            PayTypeSeeder.Seed(context);
            ActionLogSeeder.Seed(context);
            ReasonSeeder.Seed(context);
            RoleSeeder.Seed(context);
            UserSeeder.Seed(context);
        }
    }
}

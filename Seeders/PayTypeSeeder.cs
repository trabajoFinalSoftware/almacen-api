﻿using control_almacen_api.Context;
using control_almacen_api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace control_almacen_api.Seeders
{
    public class PayTypeSeeder
    {
        public static void Seed(AppDbContext context)
        {
            if (context.PayTypes.Any())
                return;

            List<PayType> payTypes = new List<PayType>
            {
                new PayType
                {
                    Id = Guid.NewGuid().ToString(),
                    StaticCode = 1,
                    Description = "Efectivo",
                    Active = true
                },
                new PayType
                {
                    Id = Guid.NewGuid().ToString(),
                    StaticCode = 2,
                    Description = "Yape",
                    Active = true
                },
                new PayType
                {
                    Id = Guid.NewGuid().ToString(),
                    StaticCode = 3,
                    Description = "Plin",
                    Active = true
                },
                new PayType
                {
                    Id = Guid.NewGuid().ToString(),
                    StaticCode = 4,
                    Description = "Visa",
                    Active = true
                },
                new PayType
                {
                    Id = Guid.NewGuid().ToString(),
                    StaticCode = 5,
                    Description = "MasterCard",
                    Active = true
                }
            };

            payTypes.ForEach(pt => context.PayTypes.Add(pt));
            context.SaveChanges();
        }
    }
}

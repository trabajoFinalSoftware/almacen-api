﻿using control_almacen_api.Context;
using control_almacen_api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace control_almacen_api.Seeders
{
    public class RoleSeeder
    {
        public static void Seed(AppDbContext context)
        {
            if (context.Roles.Any())
                return;

            List<Role> roles = new List<Role>
            {
                new Role
                {
                    Id = "1",
                    StaticCode = 1,
                    Description = "Administrador",
                    Active = true
                },
                new Role
                {
                    Id = "2",
                    StaticCode = 2,
                    Description = "Empleado",
                    Active = true
                }
            };

            roles.ForEach(r => context.Roles.Add(r));
            context.SaveChanges();
        }
    }
}

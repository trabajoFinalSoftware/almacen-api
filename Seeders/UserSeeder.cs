﻿using control_almacen_api.Context;
using control_almacen_api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace control_almacen_api.Seeders
{
    public class UserSeeder
    {
        public static void Seed(AppDbContext context)
        {
            if (context.Users.Any())
                return;
            List<User> users = new List<User>
            {
                new User
                {
                    Id = Guid.NewGuid().ToString(),
                    Nick = "admin",
                    Password = BCrypt.Net.BCrypt.HashPassword("1234"),
                    FirstName = "Admin",
                    LastName = "",
                    RoleId = "1",
                    Active = true
                },
                new User
                {
                    Id = Guid.NewGuid().ToString(),
                    Nick = "user",
                    Password = BCrypt.Net.BCrypt.HashPassword("1234"),
                    FirstName = "Empleado",
                    LastName = "Prueba",
                    RoleId = "2",
                    Active = true
                }
            };

            users.ForEach(u => context.Users.Add(u));
            context.SaveChanges();
        }
    }
}

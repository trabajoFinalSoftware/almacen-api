﻿using control_almacen_api.Context;
using control_almacen_api.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace control_almacen_api.Seeders
{
    public class ReasonSeeder
    {
        public static void Seed(AppDbContext context)
        {
            if (context.Reasons.Any())
                return;

            List<Reason> reasons = new List<Reason>
            {
                new Reason
                {
                    Id = Guid.NewGuid().ToString(),
                    ActionLogId = "1",
                    StaticCode = 1,
                    Description = "Nuevo",
                    Active = true
                },
                new Reason
                {
                    Id = Guid.NewGuid().ToString(),
                    ActionLogId = "1",
                    StaticCode = 2,
                    Description = "Re-Stock",
                    Active = true
                },
                new Reason
                {
                    Id = Guid.NewGuid().ToString(),
                    ActionLogId = "1",
                    StaticCode = 3,
                    Description = "Devolucion",
                    Active = true
                },
                new Reason
                {
                    Id = Guid.NewGuid().ToString(),
                    ActionLogId = "2",
                    StaticCode = 4,
                    Description = "Venta",
                    Active = true
                },
                new Reason
                {
                    Id = Guid.NewGuid().ToString(),
                    ActionLogId = "2",
                    StaticCode = 5,
                    Description = "Caducidad",
                    Active = true
                },
                new Reason
                {
                    Id = Guid.NewGuid().ToString(),
                    ActionLogId = "2",
                    StaticCode = 6,
                    Description = "Devolucion a proveedor",
                    Active = true
                }
            };

            reasons.ForEach(r => context.Reasons.Add(r));
            context.SaveChanges();
        }
    }
}

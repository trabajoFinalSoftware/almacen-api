﻿using Microsoft.EntityFrameworkCore;
using control_almacen_api.Domain;

namespace control_almacen_api.Context
{
    public class AppDbContext : DbContext
    {
        public DbSet<ActionLog> ActionLogs { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Reason> Reasons { get; set; }
        public DbSet<PayType> PayTypes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductLog> ProductLogs{ get; set; }
        public DbSet<ProductOrder> ProductOrders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //var contextOptions = optionsBuilder.UseNpgsql("User ID=postgres; Password=1234; Host=localhost; Port=5432; Database=almacen; Pooling=true");
            var contexOptions = optionsBuilder.UseSqlServer("Server=tcp:testingunp.database.windows.net,1433;Initial Catalog=testingunp;Persist Security Info=False;User ID=testingunp;Password=testunp123.;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            base.OnConfiguring(optionsBuilder);
        }
    }
}

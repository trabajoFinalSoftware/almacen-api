﻿using control_almacen_api.Context;

namespace control_almacen_api.Middleware
{
    public interface IJwtAuthenticationMiddleware
    {
        string Authenticate(AppDbContext context, string username, string password);
    }
}

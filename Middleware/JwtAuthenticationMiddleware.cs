﻿using System;
using System.Linq;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using BC = BCrypt.Net.BCrypt;
using control_almacen_api.Context;
using control_almacen_api.Utils;

namespace control_almacen_api.Middleware
{
    public class JwtAuthenticationMiddleware : IJwtAuthenticationMiddleware
    {
        private readonly IConfiguration _configuration;
        private readonly string key = "TraBaJo.InGenieria2022.";

        public JwtAuthenticationMiddleware(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string Authenticate(AppDbContext context, string nick, string password)
        {
            var user = context.Users.SingleOrDefault(x => x.Nick == nick && x.Active);

            if (user != null)
            {
                if (BC.Verify(password, user.Password))
                {
                    var encodeKey = Encoding.ASCII.GetBytes(key);
                    var data = AutentificacionUtil.getToken(user.RoleId, user.Id);
                    var clients = new ClaimsIdentity();
                    clients.AddClaim(new Claim(ClaimTypes.Name, data));
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = clients,
                        Expires = DateTime.UtcNow.AddHours(24),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(encodeKey), SecurityAlgorithms.HmacSha256Signature)
                    }; 

                    var tokenHandler = new JwtSecurityTokenHandler();
                    var token = tokenHandler.CreateToken(tokenDescriptor);

                    return tokenHandler.WriteToken(token);
                }
            }
            return null;
        }
    }
}

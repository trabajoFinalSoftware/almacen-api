﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace control_almacen_api.Domain
{
    public class PayType : BaseModel
    {
        public int StaticCode { get; set; }
        [Column(TypeName = "varchar(36)")]
        public string Description { get; set; }
    }
}

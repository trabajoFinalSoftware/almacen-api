﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace control_almacen_api.Domain
{
    public class Product : BaseModel
    {
        [Column(TypeName = "varchar(255)")]
        public string Name { get; set; }
        public DateTime ExpiryDate { get; set; }

        [Column(TypeName = "decimal(12,5)")]
        public decimal Price { get; set; }
        public int Stock { get; set; }
        [Column(TypeName = "varchar(36)")]
        public string ProviderId { get; set; }
        public Provider Provider { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace control_almacen_api.Domain
{
    public class ProductLog : BaseModel
    {
        [Column(TypeName = "varchar(36)")]
        public string ProductID { get; set; }
        public Product Product { get; set; }

        [Column(TypeName = "varchar(36)")]
        public string UserId { get; set; }
        public User User { get; set; }

        [Column(TypeName = "varchar(36)")]
        public string ActionLogId { get; set; }
        public ActionLog ActionLog { get; set; }

        [Column(TypeName = "varchar(36)")]
        public string ReasonId { get; set; }
        public Reason Reason { get; set; }

        [Column(TypeName = "varchar(36)")]
        public string OrderId { get; set; }
        public Order Order { get; set; }

        public int Amount { get; set; }
        
        public decimal Price { get; set; }

        public bool IsDeleteAction { get; set; } = false;
    }
}

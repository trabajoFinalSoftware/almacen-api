﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace control_almacen_api.Domain
{
    public class Reason : BaseModel
    {
        public int StaticCode { get; set; }

        [Column(TypeName = "varchar(36)")]
        public string ActionLogId { get; set; }
        public ActionLog ActionLog { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string Description { get; set; }
    }
}

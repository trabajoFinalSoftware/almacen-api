﻿using System.ComponentModel.DataAnnotations.Schema;

namespace control_almacen_api.Domain
{
    public class User : BaseModel
    {
        [Column(TypeName = "varchar(255)")]
        public string Nick { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Password { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string FirstName { get; set; }
        [Column(TypeName = "varchar(36)")]
        public string LastName { get; set; }
        [Column(TypeName = "varchar(36)")]
        public string RoleId { get; set; }
        public Role Role { get; set; }
    }
}

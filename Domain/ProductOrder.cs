﻿using System.ComponentModel.DataAnnotations.Schema;

namespace control_almacen_api.Domain
{
    public class ProductOrder : BaseModel
    {
        [Column(TypeName = "varchar(36)")]
        public string ProductId { get; set; }
        public Product Product { get; set; }

        [Column(TypeName = "varchar(36)")]
        public string OrderId { get; set; }
        public Order Order { get; set; }

        public int amount { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace control_almacen_api.Domain
{
    public class Provider : BaseModel
    {
        [Column(TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Adress { get; set; }
        [Column(TypeName = "varchar(9)")]
        public string Phone { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Email { get; set; }
    }
}

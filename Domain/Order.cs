﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace control_almacen_api.Domain
{
    public class Order : BaseModel
    {
        public DateTime Date { get; set; }

        [Column(TypeName = "varchar(36)")]
        public string UserId { get; set; }
        public User User { get; set; }

        [Column(TypeName = "varchar(36)")]
        public string PayTypeId { get; set; }
        public PayType PayType { get; set; }

        public int Total { get; set; }

        public List<ProductOrder> ProductOrders { get; set; }
    }
}

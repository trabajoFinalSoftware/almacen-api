﻿using System.ComponentModel.DataAnnotations.Schema;

namespace control_almacen_api.Domain
{
    public class ActionLog : BaseModel
    {
        public int StaticCode { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Description { get; set; }
    }
}

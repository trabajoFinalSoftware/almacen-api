﻿namespace control_almacen_api.Domain
{
    public class ProductDto
    {
        public Product Product { get; set; }
        public string UserId { get; set; }
        public string ActionLogId { get; set; }
        public string ReasonId { get; set; }
        public int Amount { get; set; }
    }
}

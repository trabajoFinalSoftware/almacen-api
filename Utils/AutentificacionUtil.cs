﻿namespace control_almacen_api.Utils
{
    public static class AutentificacionUtil
    {
        public static string getToken(string profileId, string userId)
        {
            return $"{profileId}/{userId}";
        }

        public static string getUserId(string token)
        {
            if (!string.IsNullOrWhiteSpace(token))
            {
                var array = token.Split("/");
                if (array.Length > 1)
                {
                    return array[1];
                }
            }
            return null;
        }

        public static string getProfileId(string token)
        {
            if (!string.IsNullOrWhiteSpace(token))
            {
                var array = token.Split("/");
                if (array.Length > 0)
                {
                    return array[0];
                }
            }
            return null;
        }
    }
}
﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using control_almacen_api.Context;
using control_almacen_api.Domain;
using control_almacen_api.Models.Response;

namespace control_almacen_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProvidersController : ControllerBase
    {
        private readonly AppDbContext _context;

        public ProvidersController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Providers
        [HttpGet("all/{page}/{limit}")]
        public async Task<HttpResponse> GetProviders([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<Provider> queryModel = _context.Providers.Where(x => x.Active);

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.Name, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.Adress, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.Phone, "%" + _search + "%")));

            var providersList = await queryModel
                                .OrderByDescending(o => o.Name)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = providersList, Total = total };
        }

        // GET: api/Providers/5
        [HttpGet("{id}")]
        public async Task<HttpResponse> GetProvider(string id)
        {
            var provider = await _context.Providers.Where(x => x.Active == true && x.Id == id).FirstOrDefaultAsync();

            if (provider == null)
            {
                return new HttpErrorResponse { Error = "No se encontraron resultados" };
            }
            return new HttpObjectResponse { Result = provider };
        }

        // PUT: api/Providers/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<HttpResponse> PutProvider(Provider provider)
        {
            var providerOld = await _context.Providers.FindAsync(provider.Id);

            provider.CreateDate = providerOld.CreateDate;

            _context.Entry(providerOld).State = EntityState.Detached;

            _context.Entry(provider).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return new HttpErrorResponse { Error = e.Message };

            }

            return new HttpObjectResponse { Result = provider, Status = "ok" };
        }

        // POST: api/Providers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<HttpResponse> PostProvider(Provider provider)
        {
            _context.Providers.Add(provider);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProviderExists(provider.Id))
                {
                    return new HttpErrorResponse { Error = "El proveedor ya existe." };
                }
                else
                {
                    throw;
                }
            }

            return new HttpObjectResponse { Result = provider, Status = "ok" };
        }

        // DELETE: api/Providers/5
        [HttpPut("delete/{id}")]
        public async Task<HttpResponse> DeleteProvider(string id)
        {
            var provider = await _context.Providers.FindAsync(id);
            provider.Active = false;

            _context.Entry(provider).State = EntityState.Modified;

            if (provider == null)
            {
                return new HttpErrorResponse { Error = "El proveedor no existe." };
            }

            await _context.SaveChangesAsync();

            return new HttpObjectResponse { Result = provider, Status = "ok" };
        }

        private bool ProviderExists(string id)
        {
            return _context.Providers.Any(e => e.Id == id);
        }
    }
}

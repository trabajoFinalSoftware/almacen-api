﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using control_almacen_api.Context;
using control_almacen_api.Domain;
using control_almacen_api.Models.Response;

namespace control_almacen_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly AppDbContext _context;

        public OrdersController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Orders
        [HttpGet("all/{page}/{limit}")]
        public async Task<HttpResponse> GetOrders([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<Order> queryModel = _context.Orders.Where(x => x.Active);

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.PayType.Description, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.User.Nick, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.User.FirstName, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.User.LastName, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.User.Role.Description, "%" + _search + "%")));

            var orderList = await queryModel
                                .OrderByDescending(o => o.CreateDate)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .Include(r => r.PayType)
                                .Include(r => r.User)
                                .Include(r => r.ProductOrders).ThenInclude(po => po.Product)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = orderList, Total = total };
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        public async Task<HttpResponse> GetOrder(string id)
        {
            var order = await _context.Orders.Where(x => x.Active == true && x.Id == id)
                .Include(r => r.PayType)
                .Include(r => r.User)
                .Include(r => r.ProductOrders).ThenInclude(po => po.Product)
                .FirstOrDefaultAsync();

            if (order == null)
            {
                return new HttpErrorResponse { Error = "No se encontraron resultados" };
            }
            return new HttpObjectResponse { Result = order };
        }

        // PUT: api/Orders/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<HttpResponse> PutOrder(Order order)
        {
            var orderOld = await _context.Users.FindAsync(order.Id);

            order.CreateDate = orderOld.CreateDate;

            _context.Entry(orderOld).State = EntityState.Detached;

            _context.Entry(order).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return new HttpErrorResponse { Error = e.Message };

            }

            return new HttpObjectResponse { Result = order, Status = "ok" };
        }

        // POST: api/Orders
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<HttpResponse> PostOrder(Order order)
        {
            _context.Orders.Add(order);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (OrderExists(order.Id))
                {
                    return new HttpErrorResponse { Error = "La orden ya existe." };
                }
                else
                {
                    throw;
                }
            }

            return new HttpObjectResponse { Result = order, Status = "ok" };
        }

        // DELETE: api/Orders/5
        [HttpDelete("{id}")]
        public async Task<HttpResponse> DeleteOrder(string id)
        {
            var order = await _context.Users.FindAsync(id);
            order.Active = false;

            _context.Entry(order).State = EntityState.Modified;

            if (order == null)
            {
                return new HttpErrorResponse { Error = "La orden no existe." };
            }

            await _context.SaveChangesAsync();

            return new HttpObjectResponse { Result = order, Status = "ok" };
        }

        private bool OrderExists(string id)
        {
            return _context.Orders.Any(e => e.Id == id);
        }
    }
}

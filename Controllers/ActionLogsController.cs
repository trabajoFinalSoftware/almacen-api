﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using control_almacen_api.Context;
using control_almacen_api.Domain;
using control_almacen_api.Models.Response;

namespace control_almacen_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActionLogsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public ActionLogsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/ActionLogs
        [HttpGet("all/{page}/{limit}")]
        public async Task<HttpResponse> GetActionLogs([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<ActionLog> queryModel = _context.ActionLogs.Where(x => x.Active);

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.Description, "%" + _search + "%")));

            var actionLogList = await queryModel
                                .OrderByDescending(o => o.Description)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = actionLogList, Total = total };
        }

        // GET: api/ActionLogs/5
        [HttpGet("{id}")]
        public async Task<HttpResponse> GetActionLog(string id)
        {
            var actionLog = await _context.ActionLogs.Where(x => x.Active == true && x.Id == id).FirstOrDefaultAsync();

            if (actionLog == null)
            {
                return new HttpErrorResponse { Error = "No se encontraron resultados" };
            }
            return new HttpObjectResponse { Result = actionLog };
        }

        // PUT: api/ActionLogs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<HttpResponse> PutActionLog(ActionLog actionLog)
        {
            var actionLogOld = await _context.ActionLogs.FindAsync(actionLog.Id);

            actionLog.CreateDate = actionLogOld.CreateDate;

            _context.Entry(actionLogOld).State = EntityState.Detached;

            _context.Entry(actionLog).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return new HttpErrorResponse { Error = e.Message };

            }

            return new HttpObjectResponse { Result = actionLog, Status = "ok" };
        }

        // POST: api/ActionLogs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<HttpResponse> PostActionLog(ActionLog actionLog)
        {
            _context.ActionLogs.Add(actionLog);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ActionLogExists(actionLog.Id))
                {
                    return new HttpErrorResponse { Error = "La accion no existe." };
                }
                else
                {
                    throw;
                }
            }

            return new HttpObjectResponse { Result = actionLog, Status = "ok" };
        }

        // DELETE: api/ActionLogs/5
        [HttpDelete("{id}")]
        public async Task<HttpResponse> DeleteActionLog(string id)
        {
            var actionLog = await _context.ActionLogs.FindAsync(id);
            actionLog.Active = false;

            _context.Entry(actionLog).State = EntityState.Modified;

            if (actionLog == null)
            {
                return new HttpErrorResponse { Error = "La accion no existe." };
            }

            await _context.SaveChangesAsync();

            return new HttpObjectResponse { Result = actionLog, Status = "ok" };
        }

        private bool ActionLogExists(string id)
        {
            return _context.ActionLogs.Any(e => e.Id == id);
        }
    }
}

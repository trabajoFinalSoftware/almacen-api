﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using control_almacen_api.Context;
using control_almacen_api.Domain;
using control_almacen_api.Models.Response;

namespace control_almacen_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PayTypesController : ControllerBase
    {
        private readonly AppDbContext _context;

        public PayTypesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/PayTypes
        [HttpGet("all/{page}/{limit}")]
        public async Task<HttpResponse> GetPayTypes([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<PayType> queryModel = _context.PayTypes.Where(x => x.Active);

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.Description, "%" + _search + "%")));

            var payTypesList = await queryModel
                                .OrderByDescending(o => o.Description)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = payTypesList, Total = total };
        }

        // GET: api/PayTypes/5
        [HttpGet("{id}")]
        public async Task<HttpResponse> GetPayType(string id)
        {
            var payType = await _context.PayTypes.Where(x => x.Active == true && x.Id == id).FirstOrDefaultAsync();

            if (payType == null)
            {
                return new HttpErrorResponse { Error = "No se encontraron resultados" };
            }
            return new HttpObjectResponse { Result = payType };
        }

        // PUT: api/PayTypes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<HttpResponse> PutPayType(PayType payType)
        {
            var payTypeOld = await _context.PayTypes.FindAsync(payType.Id);

            payType.CreateDate = payTypeOld.CreateDate;

            _context.Entry(payTypeOld).State = EntityState.Detached;

            _context.Entry(payType).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return new HttpErrorResponse { Error = e.Message };

            }

            return new HttpObjectResponse { Result = payType, Status = "ok" };
        }

        // POST: api/PayTypes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<HttpResponse> PostPayType(PayType payType)
        {
            _context.PayTypes.Add(payType);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PayTypeExists(payType.Id))
                {
                    return new HttpErrorResponse { Error = "El tipo de pago ya existe." };
                }
                else
                {
                    throw;
                }
            }

            return new HttpObjectResponse { Result = payType, Status = "ok" };
        }

        // DELETE: api/PayTypes/5
        [HttpDelete("{id}")]
        public async Task<HttpResponse> DeletePayType(string id)
        {
            var payType = await _context.PayTypes.FindAsync(id);
            payType.Active = false;

            _context.Entry(payType).State = EntityState.Modified;

            if (payType == null)
            {
                return new HttpErrorResponse { Error = "El tipo de pago no existe." };
            }

            await _context.SaveChangesAsync();

            return new HttpObjectResponse { Result = payType, Status = "ok" };
        }

        private bool PayTypeExists(string id)
        {
            return _context.PayTypes.Any(e => e.Id == id);
        }
    }
}

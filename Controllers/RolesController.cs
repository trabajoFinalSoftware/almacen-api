﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using control_almacen_api.Context;
using control_almacen_api.Domain;
using control_almacen_api.Models.Response;

namespace control_almacen_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly AppDbContext _context;

        public RolesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Roles
        [HttpGet("all/{page}/{limit}")]
        public async Task<HttpResponse> GetRoles([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<Role> queryModel = _context.Roles.Where(x => x.Active);

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.Description, "%" + _search + "%")));

            var rolesList = await queryModel
                                .OrderByDescending(o => o.Description)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = rolesList, Total = total };
        }

        // GET: api/Roles/5
        [HttpGet("{id}")]
        public async Task<HttpResponse> GetRole(string id)
        {
            var role = await _context.Roles.Where(x => x.Active == true && x.Id == id).FirstOrDefaultAsync();

            if (role == null)
            {
                return new HttpErrorResponse { Error = "No se encontraron resultados" };
            }
            return new HttpObjectResponse { Result = role };
        }

        // PUT: api/Roles/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<HttpResponse> PutRole(Role role)
        {
            var roleOld = await _context.Users.FindAsync(role.Id);

            role.CreateDate = roleOld.CreateDate;

            _context.Entry(roleOld).State = EntityState.Detached;

            _context.Entry(role).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return new HttpErrorResponse { Error = e.Message };

            }

            return new HttpObjectResponse { Result = role, Status = "ok" };
        }

        // POST: api/Roles
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<HttpResponse> PostRole(Role role)
        {
            _context.Roles.Add(role);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RoleExists(role.Id))
                {
                    return new HttpErrorResponse { Error = "El rol ya existe." };
                }
                else
                {
                    throw;
                }
            }

            return new HttpObjectResponse { Result = role, Status = "ok" };
        }

        // DELETE: api/Roles/5
        [HttpPut("{id}")]
        public async Task<HttpResponse> DeleteRole(string id)
        {
            var role = await _context.Roles.FindAsync(id);
            role.Active = false;

            _context.Entry(role).State = EntityState.Modified;

            if (role == null)
            {
                return new HttpErrorResponse { Error = "El rol no existe." };
            }

            await _context.SaveChangesAsync();

            return new HttpObjectResponse { Result = role, Status = "ok" };
        }

        private bool RoleExists(string id)
        {
            return _context.Roles.Any(e => e.Id == id);
        }
    }
}

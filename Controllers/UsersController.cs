﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using control_almacen_api.Context;
using control_almacen_api.Domain;
using control_almacen_api.Models.Response;

namespace control_almacen_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly AppDbContext _context;

        public UsersController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Users
        [HttpGet("all/{page}/{limit}")]
        public async Task<HttpResponse> GetUsers([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<User> queryModel = _context.Users.Where(x => x.Active);

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.Nick, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.FirstName, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.LastName, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.Role.Description, "%" + _search + "%")));

            var userList = await queryModel
                                .OrderByDescending(o => o.Nick)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .Include(r => r.Role)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = userList, Total = total };
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<HttpResponse> GetUser(string id)
        {
            var user = await _context.Users.Where(x => x.Active == true && x.Id == id)
                .Include(u => u.Role)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                return new HttpErrorResponse { Error = "No se encontraron resultados" };
            }
            return new HttpObjectResponse { Result = user };
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<HttpResponse> PutUser(User user)
        {
            var userOld = await _context.Users.FindAsync(user.Id);

            var userBd = await _context.Users.Where(x => x.Active == true && x.Nick == user.Nick && x.Id != user.Id).FirstOrDefaultAsync();

            if(userBd != null)
                return new HttpErrorResponse { Error = "Ya existe un usuario con ese nick." };

            if (user.Password == "")
                user.Password = userOld.Password;

            user.CreateDate = userOld.CreateDate;

            _context.Entry(userOld).State = EntityState.Detached;

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return new HttpErrorResponse { Error = e.Message };

            }

            user.Password = "";

            return new HttpObjectResponse { Result = user, Status = "ok" };
        }

        // POST: api/Users
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<HttpResponse> PostUser(User user)
        {
            _context.Users.Add(user);

            var userBd = await _context.Users.Where(x => x.Active == true && x.Nick == user.Nick).FirstOrDefaultAsync();

            if (userBd != null)
                return new HttpErrorResponse { Error = "Ya existe un usuario con ese nick." };

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserExists(user.Id))
                {
                    return new HttpErrorResponse { Error = "El usuario ya existe." };
                }
                else
                {
                    throw;
                }
            }

            user.Password = "";

            return new HttpObjectResponse { Result = user, Status = "ok" };
        }

        // DELETE: api/Users/delete/5
        [HttpPut("delete/{id}")]
        public async Task<HttpResponse> DeleteUser(string id)
        {
            var user = await _context.Users.FindAsync(id);
            user.Active = false;

            _context.Entry(user).State = EntityState.Modified;

            if (user == null)
            {
                return new HttpErrorResponse { Error = "El usuario no existe." };
            }

            await _context.SaveChangesAsync();

            return new HttpObjectResponse { Result = user, Status = "ok" };
        }

        private bool UserExists(string id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}

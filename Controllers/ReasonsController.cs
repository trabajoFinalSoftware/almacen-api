﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using control_almacen_api.Context;
using control_almacen_api.Domain;
using control_almacen_api.Models.Response;

namespace control_almacen_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReasonsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public ReasonsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Reasons
        [HttpGet("all/{page}/{limit}")]
        public async Task<HttpResponse> GetReasons([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<Reason> queryModel = _context.Reasons.Where(x => x.Active);

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.Description, "%" + _search + "%")) ||
                                                (EF.Functions.ILike(x.ActionLog.Description, "%" + _search + "%")));

            var rolesList = await queryModel
                                .OrderByDescending(o => o.Description)
                                .Include(r => r.ActionLog)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = rolesList, Total = total };
        }

        // GET: api/Reasons
        [HttpGet("all/out/{page}/{limit}")]
        public async Task<HttpResponse> GetReasonsOut([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<Reason> queryModel = _context.Reasons.Where(x => x.Active && x.ActionLog.StaticCode == 2);

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.Description, "%" + _search + "%")));

            var rolesList = await queryModel
                                .OrderByDescending(o => o.Description)
                                .Include(r => r.ActionLog)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = rolesList, Total = total };
        }

        // GET: api/Reasons/5
        [HttpGet("{id}")]
        public async Task<HttpResponse> GetReason(string id)
        {
            var reason = await _context.Reasons.Where(x => x.Active == true && x.Id == id)
                .Include(r => r.ActionLog)
                .FirstOrDefaultAsync();

            if (reason == null)
            {
                return new HttpErrorResponse { Error = "No se encontraron resultados" };
            }
            return new HttpObjectResponse { Result = reason };
        }

        // PUT: api/Reasons/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<HttpResponse> PutReason(Reason reason)
        {
            var reasonOld = await _context.Reasons.FindAsync(reason.Id);

            reason.CreateDate = reasonOld.CreateDate;

            _context.Entry(reasonOld).State = EntityState.Detached;

            _context.Entry(reason).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return new HttpErrorResponse { Error = e.Message };

            }

            return new HttpObjectResponse { Result = reason, Status = "ok" };
        }

        // POST: api/Reasons
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<HttpResponse> PostReason(Reason reason)
        {
            _context.Reasons.Add(reason);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ReasonExists(reason.Id))
                {
                    return new HttpErrorResponse { Error = "La razon ya existe." };
                }
                else
                {
                    throw;
                }
            }

            return new HttpObjectResponse { Result = reason, Status = "ok" };
        }

        // DELETE: api/Reasons/5
        [HttpPut("delete/{id}")]
        public async Task<HttpResponse> DeleteReason(string id)
        {
            var reason = await _context.Reasons.FindAsync(id);
            reason.Active = false;

            _context.Entry(reason).State = EntityState.Modified;

            if (reason == null)
            {
                return new HttpErrorResponse { Error = "La razon no existe." };
            }

            await _context.SaveChangesAsync();

            return new HttpObjectResponse { Result = reason, Status = "ok" };
        }

        private bool ReasonExists(string id)
        {
            return _context.Reasons.Any(e => e.Id == id);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using control_almacen_api.Context;
using control_almacen_api.Middleware;
using control_almacen_api.Models.Response;
using control_almacen_api.Domain;
using control_almacen_api.DTOs;

namespace control_almacen_api.Controllers
{
    [Authorize]
    [Route("api/login")]
    [ApiController]

    public class AuthController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IJwtAuthenticationMiddleware _IJwt;

        public AuthController(AppDbContext context, IJwtAuthenticationMiddleware Ijwt)
        {
            _context = context;
            _IJwt = Ijwt;
        }

        [AllowAnonymous]
        [HttpPost]
        public HttpResponse PostAuthUser([FromBody] User bodyAuthUser)
        {

            var token = _IJwt.Authenticate(_context, bodyAuthUser.Nick, bodyAuthUser.Password);

            if (token == null)
            {
                return new HttpErrorResponse
                {
                    Error = "Usuario o contraseña incorrecta"
                };
            }

            var usuario = _context.Users
                .Include(x => x.Role)
                .FirstOrDefault(x => x.Active && (EF.Functions.ILike(x.Nick, bodyAuthUser.Nick)));

            return new HttpObjectResponse
            {
                Result = new UserAuthDto
                {
                    Token = token,
                    Usuario = usuario
                }
            };

        }

        [HttpGet]
        public async Task<HttpResponse> GetAuth()
        {
            var chain = User.Identity.Name;
            var data = chain.Split('/');

            string UserId = data[1];

            var user = await _context.Users.Where(u => u.Id == UserId).FirstOrDefaultAsync();

            if (user == null)
            {
                return new HttpErrorResponse{ Error = "Usuario no registrado ..."};
            }
            else if (!user.Active)
            {
                return new HttpErrorResponse{ Error = "Usuario no registrado ..." };
            }

            user.Password = "";

            return new HttpObjectResponse{ Result = user };

        }

    }
}

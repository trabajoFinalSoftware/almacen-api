﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using control_almacen_api.Context;
using control_almacen_api.Domain;
using control_almacen_api.Models.Response;

namespace control_almacen_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductLogsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public ProductLogsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/ProductLogs
        [HttpGet("all/{page}/{limit}")]
        public async Task<HttpResponse> GetProductLogs([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<ProductLog> queryModel = _context.ProductLogs.Where(x => x.Active);

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.User.Nick, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.User.Role.Description, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.User.FirstName, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.User.LastName, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.Reason.Description, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.Product.Name, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.ActionLog.Description, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.User.Role.Description, "%" + _search + "%")));

            var productLogList = await queryModel
                                .OrderByDescending(o => o.CreateDate)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .Include(r => r.User)
                                .Include(r => r.Reason)
                                .Include(r => r.Order)
                                .Include(r => r.Product).ThenInclude(p => p.Provider)
                                .Include(r => r.ActionLog)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = productLogList, Total = total };
        }

        [HttpGet("all/input/{page}/{limit}")]
        public async Task<HttpResponse> GetProductInputLogs([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<ProductLog> queryModel = _context.ProductLogs.Where(x => x.Active && x.ActionLogId == "1");

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.Reason.Description, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.Product.Name, "%" + _search + "%")));

            var productLogList = await queryModel
                                .OrderByDescending(o => o.CreateDate)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .Include(r => r.User)
                                .Include(r => r.Reason)
                                .Include(r => r.Order)
                                .Include(r => r.Product).ThenInclude(p => p.Provider)
                                .Include(r => r.ActionLog)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = productLogList, Total = total };
        }

        [HttpGet("all/output/{page}/{limit}")]
        public async Task<HttpResponse> GetProductOutputLogs([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<ProductLog> queryModel = _context.ProductLogs.Where(x => x.Active && x.ActionLogId == "2");

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.Reason.Description, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.Product.Name, "%" + _search + "%")));

            var productLogList = await queryModel
                                .OrderByDescending(o => o.CreateDate)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .Include(r => r.User)
                                .Include(r => r.Reason)
                                .Include(r => r.Order)
                                .Include(r => r.Product).ThenInclude(p => p.Provider)
                                .Include(r => r.ActionLog)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = productLogList, Total = total };
        }

        // GET: api/ProductLogs/5
        [HttpGet("{id}")]
        public async Task<HttpResponse> GetProductLog(string id)
        {
            var productLog = await _context.ProductLogs.Where(x => x.Active == true && x.Id == id)
                .Include(r => r.User)
                .Include(r => r.Reason)
                .Include(r => r.Order)
                .Include(r => r.Product)
                .Include(r => r.ActionLog)
                .FirstOrDefaultAsync();

            if (productLog == null)
            {
                return new HttpErrorResponse { Error = "No se encontraron resultados" };
            }
            return new HttpObjectResponse { Result = productLog };
        }

        // PUT: api/ProductLogs/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<HttpResponse> PutProductLog(ProductLog productLog)
        {
            var productLogOld = await _context.ProductLogs.FindAsync(productLog.Id);

            productLog.CreateDate = productLogOld.CreateDate;

            _context.Entry(productLogOld).State = EntityState.Detached;

            _context.Entry(productLog).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return new HttpErrorResponse { Error = e.Message };

            }

            return new HttpObjectResponse { Result = productLog, Status = "ok" };
        }

        // POST: api/ProductLogs
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<HttpResponse> PostProductLog(ProductLog productLog)
        {
            _context.ProductLogs.Add(productLog);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProductLogExists(productLog.Id))
                {
                    return new HttpErrorResponse { Error = "El registro ya existe." };
                }
                else
                {
                    throw;
                }
            }

            return new HttpObjectResponse { Result = productLog, Status = "ok" };
        }

        // DELETE: api/ProductLogs/5
        [HttpDelete("{id}")]
        public async Task<HttpResponse> DeleteProductLog(string id)
        {
            var productLog = await _context.ProductLogs.FindAsync(id);
            productLog.Active = false;

            _context.Entry(productLog).State = EntityState.Modified;

            if (productLog == null)
            {
                return new HttpErrorResponse { Error = "El registro no existe." };
            }

            await _context.SaveChangesAsync();

            return new HttpObjectResponse { Result = productLog, Status = "ok" };
        }

        private bool ProductLogExists(string id)
        {
            return _context.ProductLogs.Any(e => e.Id == id);
        }
    }
}

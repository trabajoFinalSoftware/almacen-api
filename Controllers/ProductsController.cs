﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using control_almacen_api.Context;
using control_almacen_api.Domain;
using control_almacen_api.Models.Response;

namespace control_almacen_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public ProductsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Products
        [HttpGet("all/{page}/{limit}")]
        public async Task<HttpResponse> GetProducts([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<Product> queryModel = _context.Products.Where(x => x.Active);

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.Name, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.Provider.Name, "%" + _search + "%")));

            var productList = await queryModel
                                .OrderByDescending(o => o.Name)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .Include(r => r.Provider)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = productList, Total = total };
        }

        [HttpGet("all/lowstock/{page}/{limit}")]
        public async Task<HttpResponse> GetProductsLowStock([FromQuery(Name = "search")] string _search, int page, int limit)
        {
            IQueryable<Product> queryModel = _context.Products.Where(x => x.Active && x.Stock <= 10);

            if (Request.QueryString.HasValue)
                if (_search != null)
                    queryModel = queryModel.Where(x => (EF.Functions.ILike(x.Name, "%" + _search + "%")) ||
                                                   (EF.Functions.ILike(x.Provider.Name, "%" + _search + "%")));

            var productList = await queryModel
                                .OrderByDescending(o => o.Name)
                                .Skip((page - 1) * limit)
                                .Take(limit)
                                .Include(r => r.Provider)
                                .ToListAsync();

            var total = await queryModel.CountAsync();

            return new HttpArrayResponse { Results = productList, Total = total };
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<HttpResponse> GetProduct(string id)
        {
            var product = await _context.Products.Where(x => x.Active == true && x.Id == id)
                .Include(u => u.Provider)
                .FirstOrDefaultAsync();

            if (product == null)
            {
                return new HttpErrorResponse { Error = "No se encontraron resultados" };
            }
            return new HttpObjectResponse { Result = product };
        }

        // PUT: api/Products/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        public async Task<HttpResponse> PutProduct(Product product)
        {
            var productOld = await _context.Products.FindAsync(product.Id);

            product.CreateDate = productOld.CreateDate;

            _context.Entry(productOld).State = EntityState.Detached;

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return new HttpErrorResponse { Error = e.Message };

            }

            return new HttpObjectResponse { Result = product, Status = "ok" };
        }

        [HttpPut("log")]
        public async Task<HttpResponse> PutProductLog(ProductDto obj)
        {
            ProductLog productLog = new ProductLog();
            var product = await _context.Products.FindAsync(obj.Product.Id);

            if (obj.ActionLogId == "1")
            {
                var reason = await _context.Reasons.Where(x => x.Active == true && x.StaticCode == 2).FirstOrDefaultAsync();
                productLog.ReasonId = reason.Id;
                product.Stock = product.Stock + obj.Amount;
            }
            else
            {
                product.Stock = product.Stock - obj.Amount;
                productLog.ReasonId = obj.ReasonId;
            }

            if (product.Stock < 0)
                return new HttpErrorResponse { Error = "El monto supera el stock, por favor actualice la tabla" };

            productLog.ActionLogId = obj.ActionLogId;
            productLog.Amount = obj.Amount;
            productLog.IsDeleteAction = false;
            productLog.Price = obj.Product.Price;
            productLog.UserId = obj.UserId;

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                productLog.ProductID = obj.Product.Id;

                _context.ProductLogs.Add(productLog);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException e)
            {
                return new HttpErrorResponse { Error = e.Message };

            }

            return new HttpObjectResponse { Result = product, Status = "ok" };
        }

        // POST: api/Products
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<HttpResponse> PostProduct(Product product)
        {
            _context.Products.Add(product);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProductExists(product.Id))
                {
                    return new HttpErrorResponse { Error = "El producto ya existe." };
                }
                else
                {
                    throw;
                }
            }

            return new HttpObjectResponse { Result = product, Status = "ok" };
        }

        [HttpPost("log")]
        public async Task<HttpResponse> PostProductLog(ProductDto obj)
        {
            ProductLog productLog = new ProductLog();

            productLog.ActionLogId = "1";
            productLog.Amount = obj.Product.Stock;
            productLog.IsDeleteAction = false;
            productLog.Price = obj.Product.Price;
            productLog.UserId = obj.UserId;

            var reason = await _context.Reasons.Where(x => x.Active == true && x.StaticCode == 1).FirstOrDefaultAsync();

            productLog.ReasonId = reason.Id;

            _context.Products.Add(obj.Product);
            try
            {
                productLog.ProductID = obj.Product.Id;

                _context.ProductLogs.Add(productLog);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProductExists(obj.Product.Id))
                {
                    return new HttpErrorResponse { Error = "El producto ya existe." };
                }
                else
                {
                    throw;
                }
            }

            return new HttpObjectResponse { Result = obj.Product, Status = "ok" };
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<HttpResponse> DeleteProduct(string id)
        {
            var product = await _context.Products.FindAsync(id);
            product.Active = false;
            product.Stock = 0;

            _context.Entry(product).State = EntityState.Modified;

            if (product == null)
            {
                return new HttpErrorResponse { Error = "El producto no existe." };
            }

            await _context.SaveChangesAsync();

            return new HttpObjectResponse { Result = product, Status = "ok" };
        }

        private bool ProductExists(string id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}
